﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HandwritingRecognizerApp
{
    public interface IScreenshotManager
    {
        Task<byte[]> CaptureAsync();
    }
}
