﻿using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HandwritingRecognizerApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ImageDisplayPage : ContentPage
	{
        private ImageSource _imageData;

		public ImageDisplayPage ()
		{
			InitializeComponent ();
		}
        
        public ImageDisplayPage(ImageSource imageData)
            : this()
        {
            this._imageData = imageData;
            //System.IO.Stream test = imageData.AsStream();
            //this.displayImage.Source = ImageSource.FromStream(imageData.AsStream);
        }

        protected override void OnAppearing()
        {
            //this.DisplayImage.Source = ImageSource.FromStream(this._imageData.AsStream);
            this.DisplayImage.Source = this._imageData;
        }
    }
}