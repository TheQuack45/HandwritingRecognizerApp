﻿using PCLStorage;
using Plugin.ImageEdit;
using Plugin.ImageEdit.Abstractions;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TouchTracking;
using Xamarin.Forms;

namespace HandwritingRecognizerApp.Views
{
	public partial class MainPage : ContentPage
	{
        #region Fields definition
        #region Static fields definition
        private readonly Color STROKE_COLOR = Color.Black;
        private readonly float STROKE_WIDTH;

        private const int DIGIT_TOP_Y = 932;
        private const int DIGIT_HEIGHT = 1440;
        private const int MNIST_DIGIT_WIDTH = 28;
        private const int MNIST_DIGIT_HEIGHT = 28;
        #endregion Static fields definition

        private Dictionary<long, WritingPolyline> _inProgressPolylines = new Dictionary<long, WritingPolyline>();
        private List<WritingPolyline> _completedPolyLines = new List<WritingPolyline>();
        private SKPaint _paint = new SKPaint
        {
            Style = SKPaintStyle.Stroke,
            StrokeCap = SKStrokeCap.Round,
            StrokeJoin = SKStrokeJoin.Round,
        };

        private IScreenshotManager _screenshotManager;
        private IImageEdit _imageEditor;
        #endregion Fields definition

        #region Constructors definition
        public MainPage()
		{
			InitializeComponent();

            this.STROKE_WIDTH = 80;

            this._screenshotManager = DependencyService.Get<IScreenshotManager>();
            this._imageEditor = DependencyService.Get<IImageEdit>();
		}
        #endregion Constructors definition

        #region Methods definition
        #region Event handlers
        private void OnClearButtonClicked(object sender, EventArgs e)
        {
            this._completedPolyLines.Clear();
            this.writingCanvasView.InvalidateSurface();
        }

        private async void OnIdentifyButtonClickedAsync(object sender, EventArgs e)
        {
            byte[] screenshotBytes = await this._screenshotManager.CaptureAsync();

            IEditableImage image = Plugin.ImageEdit.CrossImageEdit.Current.CreateImage(screenshotBytes);
            image.Crop(0, DIGIT_TOP_Y, image.Width, DIGIT_HEIGHT);
            image.Resize(MNIST_DIGIT_WIDTH, MNIST_DIGIT_HEIGHT);
            byte[] croppedImage = image.ToPng();

            await Navigation.PushAsync(new ImageDisplayPage(ImageSource.FromStream(() => new System.IO.MemoryStream(croppedImage))));
        }

        private void OnTouchEffectAction(object sender, TouchActionEventArgs args)
        {
            switch (args.Type)
            {
                case TouchActionType.Pressed:
                    if (!this._inProgressPolylines.ContainsKey(args.Id))
                    {
                        WritingPolyline polyline = new WritingPolyline
                        {
                            StrokeColor = STROKE_COLOR,
                            StrokeWidth = STROKE_WIDTH,
                        };
                        polyline.Path.MoveTo(this.ConvertToPixel(args.Location));

                        this._inProgressPolylines.Add(args.Id, polyline);
                        this.writingCanvasView.InvalidateSurface();
                    }
                    break;
                case TouchActionType.Moved:
                    if (this._inProgressPolylines.ContainsKey(args.Id))
                    {
                        WritingPolyline polyline = this._inProgressPolylines[args.Id];
                        polyline.Path.LineTo(this.ConvertToPixel(args.Location));
                        this.writingCanvasView.InvalidateSurface();
                    }
                    break;
                case TouchActionType.Released:
                    if (this._inProgressPolylines.ContainsKey(args.Id))
                    {
                        this._completedPolyLines.Add(this._inProgressPolylines[args.Id]);
                        this._inProgressPolylines.Remove(args.Id);
                        this.writingCanvasView.InvalidateSurface();
                    }
                    break;
                case TouchActionType.Cancelled:
                    if (this._inProgressPolylines.ContainsKey(args.Id))
                    {
                        this._inProgressPolylines.Remove(args.Id);
                        this.writingCanvasView.InvalidateSurface();
                    }
                    break;
            }
        }

        private void OnCanvasViewPaintSurface(object sender, SKPaintSurfaceEventArgs args)
        {
            SKCanvas canvas = args.Surface.Canvas;
            canvas.Clear();

            foreach (WritingPolyline polyline in this._completedPolyLines)
            {
                this._paint.Color = polyline.StrokeColor.ToSKColor();
                this._paint.StrokeWidth = polyline.StrokeWidth;

                canvas.DrawPath(polyline.Path, this._paint);
            }

            foreach (WritingPolyline polyline in this._inProgressPolylines.Values)
            {
                this._paint.Color = polyline.StrokeColor.ToSKColor();
                this._paint.StrokeWidth = polyline.StrokeWidth;

                canvas.DrawPath(polyline.Path, this._paint);
            }
        }
        #endregion Event handlers

        // TODO: Register ScreenshotManagers with dependency injection container.

        private async Task SaveToFileSystemAsync(byte[] imageData)
        {
            IFileSystem fs = FileSystem.Current;
            IFolder rootFolder = fs.LocalStorage;
            IFolder folder = await rootFolder.CreateFolderAsync("screenshots", CreationCollisionOption.OpenIfExists);

            IFile imageFile = await folder.CreateFileAsync("canvas.png", CreationCollisionOption.ReplaceExisting);
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(await imageFile.OpenAsync(FileAccess.ReadAndWrite)))
            {
                sw.Write(imageData);
            }
        }

        private async Task SaveToFileSystemAsync(SKData imageData)
        {
            IFileSystem fs = FileSystem.Current;
            IFolder rootFolder = fs.LocalStorage;

            IFile imageFile = await rootFolder.CreateFileAsync("canvas.png", CreationCollisionOption.ReplaceExisting);
            using (System.IO.Stream fileStream = await imageFile.OpenAsync(FileAccess.ReadAndWrite))
            {
                imageData.SaveTo(fileStream);
            }
        }

        private ImageSource SaveCanvas()
        {
            SKImageInfo imageInfo = new SKImageInfo(64, 64, SKImageInfo.PlatformColorType, SKAlphaType.Premul);
            SKSurface surface = SKSurface.Create(imageInfo);
            SKCanvas canvas = surface.Canvas;
            canvas.Clear();

            foreach (WritingPolyline line in this._completedPolyLines)
            {
                canvas.DrawPath(line.Path, this._paint);
            }

            canvas.Flush();

            SKImage image = surface.Snapshot();
            SKData data = image.Encode(SKEncodedImageFormat.Jpeg, 80);
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            data.SaveTo(ms);
            return ImageSource.FromStream(() => ms);
        }

        private SKPoint ConvertToPixel(Point point)
        {
            return new SKPoint((float)(this.writingCanvasView.CanvasSize.Width * point.X / this.writingCanvasView.Width),
                               (float)(this.writingCanvasView.CanvasSize.Height * point.Y / this.writingCanvasView.Height));
        }

        private float ConvertToPixel(float flt)
        {
            return (float)(this.writingCanvasView.CanvasSize.Width * flt / this.writingCanvasView.Width);
        }
        #endregion Methods definition
    }
}