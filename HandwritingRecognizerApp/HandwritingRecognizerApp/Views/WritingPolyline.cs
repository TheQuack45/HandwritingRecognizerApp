﻿using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HandwritingRecognizerApp.Views
{
    class WritingPolyline
    {
        #region Properties definition
        public SKPath Path { get; set; }
        public Color StrokeColor { get; set; }
        public float StrokeWidth { get; set; }
        #endregion Properties definition

        #region Constructors definition
        public WritingPolyline()
        {
            this.Path = new SKPath();
        }
        #endregion Constructors definition
    }
}
