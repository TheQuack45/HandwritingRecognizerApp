﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

[assembly: Xamarin.Forms.Dependency(typeof(HandwritingRecognizerApp.Droid.ScreenshotManager))]
namespace HandwritingRecognizerApp.Droid
{
    public class ScreenshotManager : IScreenshotManager
    {
        public static Activity Activity { get; set; }

        public async System.Threading.Tasks.Task<byte[]> CaptureAsync()
        {
            if (Activity == null)
            {
                throw new InvalidOperationException("You have to set ScreenshotManager.Activity in your Android project.");
            }

            View view = Activity.Window.DecorView;
            view.DrawingCacheEnabled = true;

            Bitmap bitmap = view.GetDrawingCache(true);

            byte[] bitmapData;

            using (var stream = new MemoryStream())
            {
                bitmap.Compress(Bitmap.CompressFormat.Png, 0, stream);
                bitmapData = stream.ToArray();
            }

            view.DestroyDrawingCache();
            return bitmapData;
        }
    }
}