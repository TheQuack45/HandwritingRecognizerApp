﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Plugin.ImageEdit;
using Plugin.ImageEdit.Abstractions;
using Prism;
using Prism.Ioc;

namespace HandwritingRecognizerApp.Droid
{
    [Activity(Label = "HandwritingRecognizerApp", Icon = "@mipmap/ic_launcher", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            ScreenshotManager.Activity = this;

            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App(new AndroidInitializer()));
        }
    }

    public class AndroidInitializer : IPlatformInitializer
    {
        public void RegisterTypes(IContainerRegistry container)
        {
            container.Register<IImageEdit, ImageEdit>();
            container.Register<IScreenshotManager, ScreenshotManager>();
        }
    }
}

