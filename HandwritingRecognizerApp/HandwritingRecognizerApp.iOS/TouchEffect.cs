﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using Xamarin.Forms.Platform.iOS;

namespace TouchTracking.iOS
{
    public class TouchEffect : PlatformEffect
    {
        UIView _view;
        TouchRecognizer _touchRecognizer;

        protected override void OnAttached()
        {
            // Get the iOS UIView corresponding to the Element that the effect is attached to.
            this._view = this.Control == null ? this.Container : this.Control;

            // Get access to the TouchEffect class in the .NET Standard library.
            TouchTracking.TouchEffect effect = (TouchTracking.TouchEffect)this.Element.Effects.FirstOrDefault(e => e is TouchTracking.TouchEffect);

            if (effect != null && this._view != null)
            {
                // Create a TouchRecognizer for this UIView.
                this._touchRecognizer = new TouchRecognizer(this.Element, this._view, effect);
                this._view.AddGestureRecognizer(this._touchRecognizer);
            }
        }

        protected override void OnDetached()
        {
            if (this._touchRecognizer != null)
            {
                // Clean up the TouchRecognizer object.
                this._touchRecognizer.Detach();

                // Remove the TouchRecognizer from the UIView.
                this._view.RemoveGestureRecognizer(this._touchRecognizer);
            }
        }
    }
}