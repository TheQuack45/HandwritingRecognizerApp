﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreGraphics;
using Foundation;
using UIKit;
using Xamarin.Forms;

namespace TouchTracking.iOS
{
    class TouchRecognizer : UIGestureRecognizer
    {
        Element _element; // Xamarin.Forms element for firing events.
        UIView _view; // iOS UIView.
        TouchTracking.TouchEffect _touchEffect;
        bool _capture;

        static Dictionary<UIView, TouchRecognizer> _viewDictionary = new Dictionary<UIView, TouchRecognizer>();
        static Dictionary<long, TouchRecognizer> _idToTouchDictionary = new Dictionary<long, TouchRecognizer>();

        public TouchRecognizer(Element element, UIView view, TouchTracking.TouchEffect touchEffect)
        {
            this._element = element;
            this._view = view;
            this._touchEffect = touchEffect;

            _viewDictionary.Add(view, this);
        }

        public void Detach()
        {
            _viewDictionary.Remove(this._view);
        }

        // touches = touches of interest; evt = all touches of type UITouch
        public override void TouchesBegan(NSSet touches, UIEvent evt)
        {
            base.TouchesBegan(touches, evt);

            foreach (UITouch touch in touches.Cast<UITouch>())
            {
                long id = touch.Handle.ToInt64();
                FireEvent(this, id, TouchActionType.Pressed, touch, true);

                if (!_idToTouchDictionary.ContainsKey(id))
                {
                    _idToTouchDictionary.Add(id, this);
                }
            }

            // Save the setting of the Capture property
            this._capture = this._touchEffect.Capture;
        }

        public override void TouchesMoved(NSSet touches, UIEvent evt)
        {
            base.TouchesMoved(touches, evt);

            foreach (UITouch touch in touches.Cast<UITouch>())
            {
                long id = touch.Handle.ToInt64();

                if (this._capture)
                {
                    FireEvent(this, id, TouchActionType.Moved, touch, true);
                }
                else
                {
                    CheckForBoundaryHop(touch);

                    if (_idToTouchDictionary[id] != null)
                    {
                        FireEvent(_idToTouchDictionary[id], id, TouchActionType.Moved, touch, true);
                    }
                }
            }
        }

        public override void TouchesEnded(NSSet touches, UIEvent evt)
        {
            base.TouchesEnded(touches, evt);

            foreach (UITouch touch in touches.Cast<UITouch>())
            {
                long id = touch.Handle.ToInt64();

                if (this._capture)
                {
                    FireEvent(this, id, TouchActionType.Released, touch, false);
                }
                else
                {
                    CheckForBoundaryHop(touch);

                    if (_idToTouchDictionary[id] != null)
                    {
                        FireEvent(_idToTouchDictionary[id], id, TouchActionType.Released, touch, false);
                    }
                }
                _idToTouchDictionary.Remove(id);
            }
        }

        public override void TouchesCancelled(NSSet touches, UIEvent evt)
        {
            base.TouchesCancelled(touches, evt);

            foreach (UITouch touch in touches.Cast<UITouch>())
            {
                long id = touch.Handle.ToInt64();

                if (this._capture)
                {
                    FireEvent(this, id, TouchActionType.Cancelled, touch, false);
                }
                else if (_idToTouchDictionary[id] != null)
                {
                    FireEvent(_idToTouchDictionary[id], id, TouchActionType.Cancelled, touch, false);
                }
                _idToTouchDictionary.Remove(id);
            }
        }

        void CheckForBoundaryHop(UITouch touch)
        {
            long id = touch.Handle.ToInt64();

            // TODO: Might require converting to a List for multiple hits
            TouchRecognizer recognizerHit = null;

            foreach (UIView view in _viewDictionary.Keys)
            {
                CGPoint location = touch.LocationInView(view);

                if (new CGRect(new CGPoint(), view.Frame.Size).Contains(location))
                {
                    recognizerHit = _viewDictionary[view];
                }
            }
            if (recognizerHit != _idToTouchDictionary[id])
            {
                if (_idToTouchDictionary[id] != null)
                {
                    FireEvent(_idToTouchDictionary[id], id, TouchActionType.Exited, touch, true);
                }
                if (recognizerHit != null)
                {
                    FireEvent(recognizerHit, id, TouchActionType.Entered, touch, true);
                }
                _idToTouchDictionary[id] = recognizerHit;
            }
        }

        void FireEvent(TouchRecognizer recognizer, long id, TouchActionType actionType, UITouch touch, bool isInContact)
        {
            // Convert touch location to Xamarin.Forms Point value
            CGPoint cgPoint = touch.LocationInView(recognizer.View);
            Point xfPoint = new Point(cgPoint.X, cgPoint.Y);

            // Get the method to call for firing events
            Action<Element, TouchActionEventArgs> onTouchAction = recognizer._touchEffect.OnTouchAction;

            // Call that method
            onTouchAction(recognizer._element,
                new TouchActionEventArgs(id, actionType, xfPoint, isInContact));
        }
    }
}